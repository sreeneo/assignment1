document.querySelector('.fname').addEventListener('blur', validateFirstName);
document.querySelector('.lname').addEventListener('blur', validateLastName);
document.querySelector('.age').addEventListener('blur', validateAge);
document.querySelector('.dob').addEventListener('blur', validateBirthDate);
document.querySelector('.email').addEventListener('blur', validateEmail);
//document.querySelector('.gender');
//document.querySelector('input[name="gender"]').addEventListener('click', validateGender);
document.querySelector('.state').addEventListener('blur', validateState);
//document.querySelector('input[type="checkbox"]').addEventListener('click',validateHobby);
//document.querySelector('.file').addEventListener('click', validateFile);


function validateFirstName() {
    const fname = document.querySelector('.fname');
    const re = /^[a-zA-Z]{3,20}$/;
  
    if(!re.test(fname.value) || fname.value===null){
      fname.classList.add('is-invalid');
    } 
    else {
      fname.classList.remove('is-invalid');
    }
  }

  function validateLastName(){
    const lname = document.querySelector('.lname');
  const re = /^[a-zA-Z]{2,20}$/;

  if(!re.test(lname.value) || lname.value===null){
    lname.classList.add('is-invalid');
  } 
  else {
    lname.classList.remove('is-invalid');
  }
}

function validateAge(){
    const age = document.querySelector('.age');
    const re = /^[0-9]{1,3}?$/;
    
    if(!re.test(age.value)|| age.value>100 || age.value==='' || age.value===0){
        age.classList.add('is-invalid');
      } 
    else {
        age.classList.remove('is-invalid');
      }
}

function validateBirthDate(){
    const dob=document.querySelector('.dob');

    if(dob.value==='')
    {
        dob.classList.add('is-invalid');
    }
    else{
        dob.classList.remove('is-invalid');
    }
}

function validateEmail() {
  const email = document.querySelector('.email');
  const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

  if(!re.test(email.value) || email.value===''){
    email.classList.add('is-invalid');
  } else {
    email.classList.remove('is-invalid');
  }

}


function validateState(){

    const state=document.querySelector('.state');
    
        if(state.value==='')
        {
            state.classList.add('is-invalid');
        }
        else{
            state.classList.remove('is-invalid');
        }
    }

function gen(){
    const gender=document.querySelectorAll('.gender');

    for(i=0;i<gender.length;i++){
        if(gender[i].checked==true){
            return true;
         }
         else{
        document.getElementById("message").innerHTML="Please select your gender";
         return false;
        }
    }
}

function hob(){
    const hobby=document.querySelectorAll('.hobby');

    for(i=0;i<hobby.length;i++){
        if(hobby[i].checked==true){
            return true;
         }
         else{
        document.getElementById("msg").innerHTML="Please select hobbies";
         return false;
        }
    }
}

function filefun(){
    const file=document.querySelectorAll('.file');
    var filePath = file.value; 
          
            // Allowing file type 
            var allowedExtensions = /(\.pdf)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                document.getElementById("fmsg").innerHTML="Please select PDF file";
                file.value = ''; 
                return false; 
            }  
            else{
            return true;
        }
}




    //Constructor
function Data(fname,lname,age,dob,email,gender,state,hobby,file){
    this.fname=fname;
    this.lname=lname;
    this.age=age;
    this.dob=dob;
    this.email=email;
    this.gender=gender;
    this.state=state;
    this.hobby=hobby;
    this.file=file;
}




//UI Constructor
function UI(){}

//Add data to list
UI.prototype.addDataToList=function(data){
    //console.log('helloo');
    const list=document.querySelector('.list-data');
    
    //create tr element
    const row=document.createElement('tr');
    //insert columns
    row.innerHTML=`
        <td>First Name: ${data.fname}</td>
        <td>Last Name: ${data.lname}</td>
        <td>Age: ${data.age}</td>
        <td>Date of Birth: ${data.dob}</td>
        <td>Email: ${data.email}</td>
        <td>Gender: ${data.gender}</td>
        <td>State: ${data.state}</td>
        <td>Hobby: ${data.hobby}</td>
        <td>File: ${data.file}</td>
    `;
    list.appendChild(row);

    //console.log(row);
}

//show alert
UI.prototype.showAlert=function(message,className){
    //create div
    const div=document.createElement('div');
    //add classes
    div.className=`alert ${className}`;
    //add text
    div.appendChild(document.createTextNode(message));
    //get parent
    const container=document.querySelector('.col-md-6');
    //get form
    const form=document.querySelector('#data-form');
    //insert alert
    container.insertBefore(div,form);

    //timeout
    setTimeout(function(){
        document.querySelector('.alert').remove()
    },6000);
}




//Event Listener
document.querySelector('#data-form').addEventListener('submit',
function(e){
    //Form values
const fname = document.querySelector('.fname').value;
const lname = document.querySelector('.lname').value;
const age = document.querySelector('.age').value;
const dob = document.querySelector('.dob').value;
const email = document.querySelector('.email').value;
//const gender = document.querySelector('.gender').value;
const gender =document.querySelector('input[type="radio"]:checked').value;
const state = document.querySelector('.state').value;


const checkBoxes_checked =document.querySelectorAll('input[type="checkbox"]:checked');
  console.log(checkBoxes_checked);
  let hobby_list ='';
  checkBoxes_checked.forEach((checkVal,index) =>{
    // console.log(checkVal.name);
    if(index < checkBoxes_checked.length-1){
      hobby_list += `${checkVal.value},`;
    }
    else{
      hobby_list +=checkVal.value;
    }
  });
  const hobby=hobby_list;





const file = document.querySelector('.file').value;

          //instantiate 
    const data=new Data(fname,lname,age,dob,email,gender,state,hobby,file);
        //instantiate UI
    const ui=new UI();

    //validate
if(fname===''|| lname===''||age===''||dob===''|| email===''||gender===''||state===''|| hobby===''||file===''){
    //console.log('alert');
    //alert('Failed');
    ui.showAlert('Please fill all fields','error');
}

else
{
            //Add data to list
            ui.addDataToList(data);

            //show success
            ui.showAlert('Data Added','success');

            // //Clear fields
            // ui.clearFields();
}

    e.preventDefault();
});


